import React, {useState, useEffect} from 'react';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Login() {

	// State Hooks
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState('');

	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])

	function loginUser(e) {
		e.preventDefault();
		clearFields();
		Swal.fire({
			title: "Login",
			icon: "success",
			text: "Redirecting to Homepage"
		})
	}

	function clearFields() {
		setEmail('');
		setPassword('');
	}


	return (
		<Form className="mt-3" onSubmit={(e) => loginUser(e)}>
			<h1>Login</h1>
			<Form.Group>
				<Form.Label>Email Address:</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter Email"
					required
					value={email}
					onChange={e => setEmail(e.target.value)} 
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter Password"
					required
					value={password}
					onChange={e => setPassword(e.target.value)}
				/>
			</Form.Group>
			{isActive ?
			<Button className="mt-2" variant="primary" type="submit">Login</Button>
			:
			<Button className="mt-2" variant="primary" type="submit" disabled>Login</Button>
			}
		</Form>
	)


}
