import React from 'react';
import coursesData from '../mockData/coursesData';
import CourseCard from '../components/CourseCard';

export default function Courses() {

	//Check to see if the mock data was captured
	console.log(coursesData[0]);


	//for us to be able to display all the courses from the data file, we are going to use map()
	//The "map" method loops through all the individual course objects in our array and returns a component for each course.

	//Multiple components created through the map method must have a UNIQUE KEY that will help React JS identify which components/elements have been changed, added or removed.
	const courses = coursesData.map(course => {
		return(
			<CourseCard key={course.id} courseProp={course}/>

			)
	})

	return (
		<>
			<h1>Courses</h1>
			{courses}
		</>

		)
}
