import React from 'react';
import './App.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import { Container } from 'react-bootstrap';
import Register from './pages/register.js';
import Login from './pages/login.js';

function App() {
  return (
  <>
   <AppNavbar />
   <Container>
    <Home />
    <Courses />
    <Register />
    <Login />
   </Container>
  </>
  );
}

export default App;
