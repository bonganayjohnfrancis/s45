const coursesData = [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Similique aperiam nihil labore incidunt nesciunt saepe exercitationem eveniet voluptatum, soluta eos facilis excepturi ipsa consectetur impedit cum fugiat laborum deleniti sit!",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Similique aperiam nihil labore incidunt nesciunt saepe exercitationem eveniet voluptatum, soluta eos facilis excepturi ipsa consectetur impedit cum fugiat laborum deleniti sit!",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Similique aperiam nihil labore incidunt nesciunt saepe exercitationem eveniet voluptatum, soluta eos facilis excepturi ipsa consectetur impedit cum fugiat laborum deleniti sit!",
		price: 55000,
		onOffer: true
	}
]
export default coursesData;
